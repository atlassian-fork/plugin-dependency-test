# Atlassian Dependency Test Plugins

This project consists of two Jira plugins (**One Thousand Component Plugin** and **Two Thousand Component Plugin**) that has high number of modules. 

Both of these plugins depend on Atlassian UPM and **Two Thousand Component Plugin** also depends on **One Thousand Component Plugin**

These plugins are used to test effects of enabling/disabling plugins with large number dependencies.
